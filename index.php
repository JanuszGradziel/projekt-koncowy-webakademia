<?php
session_start();
if(isset($_SESSION["uid"])){
	header("location:profile.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Polsport</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="main.js"></script>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  		<a class="navbar-brand" href="index.php">Polsport</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
	  	<div class="collapse navbar-collapse" id="navbarNav">
	    	<ul class="navbar-nav">	      		
	        	<li class="nav-item">
	      			<a class="nav-link" href="#">Produkty</a>
	      		</li>
	      		<form class="form-inline" style="margin-left: 30px;">
    				<input class="form-control mr-sm-2" type="text" placeholder="Szukaj" aria-label="Search" style="width: 300px;" id="search">
    				<button class="btn btn-success" id="search_btn">Szukaj</button>
  				</form>	      	
	    	</ul>
	    	<ul class="navbar-nav ml-auto">
	      		<li class="nav-item dropdown">
	        		<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Koszyk</a>
	        		<div class="dropdown-menu dropdown-menu-right" style="width: 400px;">
	        			<div class="card bg-success">
	        				<div class="card-header">
	        					<div class="row">
	        						<div class="col-md-3">Nr</div>
	        						<div class="col-md-3">Zdjęcie</div>
	        						<div class="col-md-3">Nazwa</div>
	        						<div class="col-md-3">Cena</div>
	        					</div>	        					
	        				</div>
	        				<div class="card-body bg-light"></div>
	        				<div class="card-footer"></div>
	        			</div>
	        		</div>
	        	</li>
	        	<li class="nav-item dropdown">
	      			<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Zaloguj</a>
	      			<div class="dropdown-menu dropdown-menu-right">
   						<div style="width: 300px;">
   							<div class="card bg-primary">
   								<div class="card-header">Login</div>
   								<div class="card-body">
   									<label for="email">Email</label>
   									<input type="email" class="form-control" id="email" required>
   									<label for="email">Hasło</label>
   									<input type="password" class="form-control" id="password" required>
   									<p><br></p>
   									<a href="customer_registration.php" class="btn btn-success" style="list-style: none;">Zarejestruj się</a><input type="submit" class="btn btn-success" style="float: right;" id="login" value="Zaloguj">
   								</div>
   								<div class="card-footer" id="e_msg"></div>
   							</div>
   						</div>
	      			</div>	      				      			
	      		</li>	      		
	      	</ul>
	  	</div>
	</nav>
	<p><br></p>
	<p><br></p>
	<p><br></p>	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div id="get_category">					
				</div>
			<!--<nav class="nav flex-column">
				  <a class="nav-link active bg-primary text-white rounded" href="#"><h4>Kategorie</h4></a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				</nav> -->
				<div id="get_brand">					
				</div>
			<!--<nav class="nav flex-column">
				  <a class="nav-link active bg-primary text-white rounded" href="#"><h4>Brands</h4></a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				</nav>-->
			</div>
			<div class="col-md-8">
				<div class="card bg-success">
					<div class="card-header">Produkty</div>
					<div class="card-body bg-light">
						<div id="get_product">
							<!-- product jquery Ajax Request -->							
						</div>
					<!--<div class="col-md-4">
							<div class="card bg-info">
								<div class="card-header bg-success">Piłka</div>
								<div class="card-body bg-light">
									<img src="images/nike.jpg" style="width: 280px; height: 186px;">
								</div>
								<div class="card-header bg-success">500.00 zł
									<button style="float: right;" class="btn btn-danger btn-sm">AddToCard</button>
								</div>
							</div>
						</div>-->
					</div>
					<div class="card-footer bg-light">&copy; Janusz Grądziel 2018</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</body>
</html>