<?php

session_start();
if(!isset($_SESSION["uid"])){
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Polsport</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="main.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  		<a class="navbar-brand" href="index.php">Polsport</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>	  	
	</nav>
	<p><br></p>
	<p><br></p>
	<p><br></p>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" id="cart_msg">
				<!-- Cart Message -->
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="card bg-primary">
					<div class="card-header">Zweryfikuj zamówienie</div>
					<div class="card-body bg-white">
						<div class="row">
							<div class="col-md-2"><b>Usuń/Dodaj</b></div>
							<div class="col-md-2"><b>Zdjęcie</b></div>
							<div class="col-md-2"><b>Nazwa</b></div>
							<div class="col-md-2"><b>Ilość</b></div>
							<div class="col-md-2"><b>Cena produktu</b></div>
							<div class="col-md-2"><b>Łączna cena</b></div>
						</div>
						<div id="cart_checkout"></div>
					<!--<div class="row">
							<div class="col-md-2">
								<div class="btn-group">
									<a href="#" class="btn btn-danger"><img src="png/trash-2x.png"></a>
									<a href="#" class="btn btn-primary"><img src="png/thumb-up-2x.png"></a>
								</div>
							</div>
							<div class="col-md-2"><img src="images/"></div>
							<div class="col-md-2">Product Name</div>
							<div class="col-md-2"><input type="text" class="form-control" value="1"></div>
							<div class="col-md-2"><input type="text" class="form-control" value="5000" disabled></div>
							<div class="col-md-2"><input type="text" class="form-control" value="5000" disabled></div>
						</div>-->
					<!--<div class="row">
							<div class="col-md-8"></div>
							<div class="col-md-4">
								<b>Łącznie $500000</b>
							</div>
						</div>-->
						
					</div>
					<div class="card-footer bg-white">&copy; Janusz Grądziel 2018</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>
</html>