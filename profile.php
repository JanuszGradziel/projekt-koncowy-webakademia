<?php

session_start();
if(!isset($_SESSION["uid"])){
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Polsport</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="main.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  		<a class="navbar-brand" href="index.php">Polsport</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
	  	<div class="collapse navbar-collapse" id="navbarNav">
	    	<ul class="navbar-nav">	      		
	        	<li class="nav-item">
	      			<a class="nav-link" href="#">Produkty</a>
	      		</li>
	      		<form class="form-inline" style="margin-left: 30px;">
    				<input class="form-control mr-sm-2" type="text" placeholder="Szukaj" aria-label="Search" style="width: 300px;" id="search">
    				<button class="btn btn-success" id="search_btn">Szukaj</button>
  				</form>	      	
	    	</ul>
	    	<ul class="navbar-nav ml-auto">
	      		<li class="nav-item dropdown">
	        		<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" id="cart_container">Koszyk<span class="badge">0</span></a>
	        		<div class="dropdown-menu dropdown-menu-right" style="width: 400px;">
	        			<div class="card bg-success">
	        				<div class="card-header">
	        					<div class="row">
	        						<div class="col-md-3">Nr</div>
	        						<div class="col-md-3">Zdjęcie</div>
	        						<div class="col-md-3">Nazwa</div>
	        						<div class="col-md-3">Cena</div>
	        					</div>	        					
	        				</div>
	        				<div class="card-body bg-light">
	        					<div id="cart_product">
		        				<!--<div class="row">
		        						<div class="col-md-3">Nr</div>
		        						<div class="col-md-3">Zdjęcie</div>
		        						<div class="col-md-3">Nazwa</div>
		        						<div class="col-md-3">Cena</div>
		        					</div>-->
	        					</div>
	        					<hr>
	        					<div style="font-size: 30px; text-align: center; ">
	        					<a style="text-decoration: none;" href="cart.php"><img src="png/dollar-4x.png">Przejdź do płatności<img src="png/dollar-4x.png"></a>
	        					</div>
	        				</div>
	        				<div class="card-footer bg-light"></div>
	        			</div>
	        		</div>
	        	</li>
	        	<li class="nav-item dropdown">
	      			<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><?php echo "Witaj ".$_SESSION["name"]; ?></a>
	      			<div class="dropdown-menu dropdown-menu-right">
   						<div style="width: 300px;">
   							<div class="card bg-success">
   								<label><a href="cart.php" style="text-decoration: none; color: black;" ><b>Koszyk</b></a></label>
   								<label class="dropdown-divider"></label>
   								<label><a href="logout.php" style="text-decoration: none; color: black;"><b>Wyloguj</b></a></label>
   							</div>
   						</div>
	      			</div>	      				      			
	      		</li>	      			
	      	</ul>
	  	</div>
	</nav>
	<p><br></p>
	<p><br></p>
	<p><br></p>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div id="get_category">					
				</div>
			<!--<nav class="nav flex-column">
				  <a class="nav-link active bg-primary text-white rounded" href="#"><h4>Kategorie</h4></a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				</nav> -->
				<div id="get_brand">					
				</div>
			<!--<nav class="nav flex-column">
				  <a class="nav-link active bg-primary text-white rounded" href="#"><h4>Brands</h4></a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				  <a class="nav-link" href="#">Categories</a>
				</nav>-->
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12" id="product_msg"></div>
				</div>
				<div class="card bg-success">
					<div class="card-header">Produkty</div>
					<div class="card-body bg-light">
						<div id="get_product">
							<!-- product jquery Ajax Request -->							
						</div>
					<!--<div class="col-md-4">
							<div class="card bg-info">
								<div class="card-header bg-success">Piłka</div>
								<div class="card-body bg-light">
									<img src="images/nike.jpg" style="width: 280px; height: 186px;">
								</div>
								<div class="card-header bg-success">500.00 zł
									<button style="float: right;" class="btn btn-danger btn-sm">AddToCard</button>
								</div>
							</div>
						</div>-->
					</div>
					<div class="card-footer bg-light">&copy; Janusz Grądziel 2018</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>		
		<div class="row">
			<div class="col-md-12">
				<center>
					<nav aria-label="Page navigation example">
						<ul class="pagination justify-content-center" id="pageno">
							<li class="page-item"><a class="page-link" href="#">1</a></li>
													
						</ul>
					</nav>
				</center>
			</div>
		</div>
	</div>	
	
	
</body>
</html>